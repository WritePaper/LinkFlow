\begin{thebibliography}{10}
\providecommand{\url}[1]{\texttt{#1}}
\providecommand{\urlprefix}{URL }

\bibitem{url-collude}
Collusion Apps a new threat,
  \url{http://eandt.theiet.org/news/2014/feb/colluding-apps.cfm}

\bibitem{url-intent}
Intents and intent filters,
  \url{http://developer.android.com/guide/components/intents-filters.html}

\bibitem{url-mongo}
MongoDB, \url{https://www.mongodb.org/}

\bibitem{url-virushare}
VirusShare, \url{https://virusshare.com/}

\bibitem{url-dropbox}
Vulnerability of Dropbox SDK,
  \url{http://www.slideshare.net/ibmsecurity/remote-exploitation-of-the-dropbox-sdk-for-android}

\bibitem{hare}
Aafer, Y., Zhang, N., Zhang, Z., Zhang, X., Chen, K., Wang, X., Zhou, X., Du,
  W., Grace, M.: Hare hunting in the wild android: A study on the threat of
  hanging attribute references. In: CCS. pp. 1248--1259 (2015)

\bibitem{flowdroid}
Arzt, S., Rasthofer, S., Fritz, C., Bodden, E., Bartel, A., Klein, J.,
  Le~Traon, Y., Octeau, D., McDaniel, P.: Flowdroid: Precise context, flow,
  field, object-sensitive and lifecycle-aware taint analysis for android apps.
  In: PLDI. vol.~49, pp. 259--269 (2014)

\bibitem{pscout}
Au, K.W.Y., Zhou, Y.F., Huang, Z., Lie, D.: Pscout: analyzing the android
  permission specification. In: CCS. pp. 217--228 (2012)

\bibitem{dexpler}
Bartel, A., Klein, J., Le~Traon, Y., Monperrus, M.: Dexpler: converting android
  dalvik bytecode to jimple for static analysis with soot. In: SOAP. pp. 27--38
  (2012)

\bibitem{bugiel2011xmandroid}
Bugiel, S., Davi, L., Dmitrienko, A., Fischer, T., Sadeghi, A.R.: Xmandroid: A
  new android evolution to mitigate privilege escalation attacks. Technische
  Universit{\"a}t Darmstadt, Technical Report TR-2011-04  (2011)

\bibitem{bugiel2012towards}
Bugiel, S., Davi, L., Dmitrienko, A., Fischer, T., Sadeghi, A.R., Shastry, B.:
  Towards taming privilege-escalation attacks on android. In: NDSS (2012)

\bibitem{comdroid}
Chin, E., Felt, A.P., Greenwood, K., Wagner, D.: Analyzing inter-application
  communication in android. In: Mobisys. pp. 239--252 (2011)

\bibitem{permission2}
Chin, E., Felt, A.P., Greenwood, K., Wagner, D.: Privilege escalation attacks
  on android. In: Mobisys. pp. 239--252 (2011)

\bibitem{need}
Elish, K.O., Yao, D., Ryder, B.G.: On the need of precise inter-app icc
  classification for detecting android malware collusions. In: MoST (2015)

\bibitem{taintdroid}
Enck, W., Gilbert, P., Han, S., Tendulkar, V., Chun, B.G., Cox, L.P., Jung, J.,
  McDaniel, P., Sheth, A.N.: Taintdroid: an information-flow tracking system
  for realtime privacy monitoring on smartphones. In: OSDI (2011)

\bibitem{permission1}
Felt, A.P., Wang, H.J., Moshchuk, A., Hanna, S., Chin, E.: Permission
  re-delegation: Attacks and defenses. In: USENIX Security. vol.~30 (2011)

\bibitem{scandroid}
Fuchs, A.P., Chaudhuri, A., Foster, J.S.: Scandroid: Automated security
  certification of android. Technical report, University of Maryland  (2009)

\bibitem{grace2012unsafe}
Grace, M.C., Zhou, W., Jiang, X., Sadeghi, A.R.: Unsafe exposure analysis of
  mobile in-app advertisements. In: WiSec. pp. 101--112 (2012)

\bibitem{permission3}
Grace, M.C., Zhou, Y., Wang, Z., Jiang, X.: Systematic detection of capability
  leaks in stock android smartphones. In: NDSS (2012)

\bibitem{flow1}
Holavanalli, S., Manuel, D., Nanjundaswamy, V., Rosenberg, B., Shen, F., Ko,
  S.Y., Ziarek, L.: Flow permissions for android. In: ASE. pp. 652--657 (2013)

\bibitem{scandal}
Kim, J., Yoon, Y., Yi, K., Shin, J., Center, S.: Scandal: Static analyzer for
  detecting privacy leaks in android applications. MoST  12 (2012)

\bibitem{didfail}
Klieber, W., Flynn, L., Bhosale, A., Jia, L., Bauer, L.: Android taint flow
  analysis for app sets. In: SOAP. pp. 1--6 (2014)

\bibitem{soot}
Lam, P., Bodden, E., Lhot{\'a}k, O., Hendren, L.: The soot framework for java
  program analysis: a retrospective. In: CETUS 2011 (2011)

\bibitem{spark}
Lhot{\'a}k, O., Hendren, L.: Scaling java points-to analysis using spark. In:
  Compiler Construction. pp. 153--169. Springer (2003)

\bibitem{iccta}
Li, L., Bartel, A., Bissyand{\'e}, T.F., Klein, J., Le~Traon, Y., Arzt, S.,
  Rasthofer, S., Bodden, E., Octeau, D., McDaniel, P.: Iccta: Detecting
  inter-component privacy leaks in android apps. In: ICSE. pp. 280--291 (2015)

\bibitem{li2014detecting}
Li, L., Bartel, A., Klein, J., Le~Traon, Y.: Detecting privacy leaks in android
  apps. In: ESSoS-DS (2014)

\bibitem{lu2012chex}
Lu, L., Li, Z., Wu, Z., Lee, W., Jiang, G.: Chex: statically vetting android
  apps for component hijacking vulnerabilities. In: CCS. pp. 229--240 (2012)

\bibitem{ic3}
Octeau, D., Luchaup, D., Dering, M., Jha, S., McDaniel, P.: Composite constant
  propagation: Application to android inter-component communication analysis.
  In: ICSE. pp. 77--88 (2015)

\bibitem{epicc}
Octeau, D., McDaniel, P., Jha, S., Bartel, A., Bodden, E., Klein, J., Le~Traon,
  Y.: Effective inter-component communication mapping in android with epicc: An
  essential step towards holistic security analysis. In: USENIX Security. pp.
  77--88 (2013)

\bibitem{susi}
Rasthofer, S., Arzt, S., Bodden, E.: A machine-learning approach for
  classifying and categorizing android sources and sinks. In: NDSS (2014)

\bibitem{reina2013system}
Reina, A., Fattori, A., Cavallaro, L.: A system call-centric analysis and
  stimulation technique to automatically reconstruct android malware behaviors.
  EuroSec, April  (2013)

\bibitem{reps1995precise}
Reps, T., Horwitz, S., Sagiv, M.: Precise interprocedural dataflow analysis via
  graph reachability. In: POPL. pp. 49--61. ACM (1995)

\bibitem{droidtrack}
Sakamoto, S., Okuda, K., Nakatsuka, R., Yamauchi, T.: Droidtrack: Tracking and
  visualizing information diffusion for preventing information leakage on
  android. JISIS  4(2),  55--69 (2014)

\bibitem{effectiveness}
Sarwar, G., Mehani, O., Boreli, R., Kaafar, M.A.: On the effectiveness of
  dynamic taint analysis for protecting against private information leaks on
  android-based devices. In: SECRYPT. pp. 461--468 (2013)

\bibitem{soundcomber}
Schlegel, R., Zhang, K., Zhou, X.y., Intwala, M., Kapadia, A., Wang, X.:
  Soundcomber: A stealthy and context-aware sound trojan for smartphones. In:
  NDSS. vol.~11, pp. 17--33 (2011)

\bibitem{flow2}
Shen, F., Vishnubhotla, N., Todarka, C., Arora, M., Dhandapani, B., Lehner,
  E.J., Ko, S.Y., Ziarek, L.: Information flows as a permission mechanism. In:
  ASE. pp. 515--526 (2014)

\bibitem{copperdroid}
Tam, K., Khan, S.J., Fattori, A., Cavallaro, L.: Copperdroid: Automatic
  reconstruction of android malware behaviors. In: NDSS (2015)

\bibitem{tripp2014bayesian}
Tripp, O., Rubin, J.: A bayesian approach to privacy enforcement in
  smartphones. In: USENIX Security. pp. 175--190 (2014)

\bibitem{amandroid}
Wei, F., Roy, S., Ou, X., et~al.: Amandroid: A precise and general
  inter-component data flow analysis framework for security vetting of android
  apps. In: CCS. pp. 1329--1341. ACM (2014)

\bibitem{intentfuzzer}
Yang, K., Zhuge, J., Wang, Y., Zhou, L., Duan, H.: Intentfuzzer: detecting
  capability leaks of android applications. In: ASIACCS. pp. 531--536 (2014)

\bibitem{leakminer}
Yang, Z., Yang, M.: Leakminer: Detect information leakage on android with
  static taint analysis. In: WCSE. pp. 101--104 (2012)

\bibitem{appsealer}
Zhang, M., Yin, H.: Appsealer: Automatic generation of vulnerability-specific
  patches for preventing component hijacking attacks in android applications.
  In: NDSS (2014)

\bibitem{zhou-Mal}
Zhou, Y., Jiang, X.: Dissecting android malware: Characterization and
  evolution. In: Security and Privacy. pp. 95--109 (2012)

\end{thebibliography}
